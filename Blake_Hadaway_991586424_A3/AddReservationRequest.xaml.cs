﻿using Blake_Hadaway_991586424_A3.Stuff;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Blake_Hadaway_991586424_A3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddReservationRequest : Page
    {

        private ObservableCollection<BookingRequest> _requests;

        private MeetingRoom _currentRoom;

        public AddReservationRequest()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            PageInformationPass passedInfoFragment = e.Parameter as PageInformationPass;
            _requests = passedInfoFragment.InfomationFragment1;
            _currentRoom = passedInfoFragment.InfomationFragment2;
            imageOutput.Source = new BitmapImage(new Uri($"ms-appx:///assets/Pictures/{_currentRoom._roomLayout}.png"));
        }

        private void ViewMeetingRoomClick(object sender, RoutedEventArgs e)
        {
            // send to the viewing of the reservations
            Frame.Navigate(typeof(ViewMeetingRooms), _requests);
        }

        private void AddReservationClick(object sender, RoutedEventArgs e)
        {
            try
            {
                BookingRequest book = GetReservationInfo();
                _requests.Add(book);
                Frame.Navigate(typeof(ViewReservationRequests), _requests);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private BookingRequest GetReservationInfo()
        {
            string roomNum = _currentRoom._roomNumber;

            int seatingCapacity = _currentRoom._seatingCapacity;

            RoomLayoutType.RoomLayoutTypeEnum room = _currentRoom._roomLayout;

            string requestedBy = nameInput.Text;

            string purpose = descriptionInput.Text;

            int numberOfPeople = int.Parse(numOfPeopleInput.Text);

            DateTime sDate = StartDate.Date.Date;

            TimeSpan startTime = StartTime.Time;

            TimeSpan endTime = EndTime.Time;

            BookingRequest book = new BookingRequest(roomNum, seatingCapacity, room, requestedBy, purpose, numberOfPeople, sDate, startTime, endTime);

            return book;
        }

        
    }
}
