﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Blake_Hadaway_991586424_A3.Stuff;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Blake_Hadaway_991586424_A3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewReservationRequests : Page, INotifyPropertyChanged
    {

        private BookingRequest _currentRoom;
        
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<BookingRequest> _requests; 

        public ViewReservationRequests()
        {
            this.InitializeComponent();
        }

        public BookingRequest CurrentRoom
        {
            get => _currentRoom;
            set
            {
                if (_currentRoom != value)
                {
                    _currentRoom = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs("CurrentProduct"));
                }
            }
        }

        private void ClickMeetingRooms(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ViewMeetingRooms), _requests);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _requests = (ObservableCollection<BookingRequest>)e.Parameter;
            if (_requests.Count > 0)
                CurrentRoom = _requests[0];
        }

    }
}
