﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Blake_Hadaway_991586424_A3.Stuff;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Blake_Hadaway_991586424_A3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewMeetingRooms : Page, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<MeetingRoom> _rooms = new ObservableCollection<MeetingRoom>();

        private ObservableCollection<BookingRequest> _bookings = new ObservableCollection<BookingRequest>();

        private MeetingRoom _currentRoom;

        public MeetingRoom CurrentRoom
        {
            get => _currentRoom;
            set
            {
                if (_currentRoom != value)
                {
                    _currentRoom = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs("CurrentRoom"));
                }
            }
        }

        public ViewMeetingRooms()
        {
            this.InitializeComponent();

            _rooms.Add(new MeetingRoom("101", 20, (RoomLayoutType.RoomLayoutTypeEnum)0));
            _rooms.Add(new MeetingRoom("202", 20, (RoomLayoutType.RoomLayoutTypeEnum)1));
            _rooms.Add(new MeetingRoom("303", 40, (RoomLayoutType.RoomLayoutTypeEnum)2));
            _rooms.Add(new MeetingRoom("404", 40, (RoomLayoutType.RoomLayoutTypeEnum)3));

            CurrentRoom = _rooms[0];

        }

        private void NavigateToClick(object sender, RoutedEventArgs e)
        {
            if (sender == AddReservationButton)
            {
                PageInformationPass InformationFragment = new PageInformationPass();
                InformationFragment.InfomationFragment1 = _bookings;
                InformationFragment.InfomationFragment2 = CurrentRoom;

                Frame.Navigate(typeof(AddReservationRequest), InformationFragment);
            }

            else if(sender == ViewReservationButton)
            {
                Frame.Navigate(typeof(ViewReservationRequests), _bookings);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter.GetType() == typeof(ObservableCollection<BookingRequest>))
                _bookings = (ObservableCollection<BookingRequest>)e.Parameter;
        }

        private void meetingRoomList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            imageOutput.Source = new BitmapImage(new Uri($"ms-appx:///assets/Pictures/{_currentRoom._roomLayout}.png"));
        }
    }
}
