﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Windows.ApplicationModel.Store.Preview.InstallControl;
using Blake_Hadaway_991586424_A3;
using Blake_Hadaway_991586424_A3.Stuff;

namespace Blake_Hadaway_991586424_A3.Stuff
{
    public class BookingRequest : MeetingRoom
    {
        private List<BookingRequest> _requestList = new List<BookingRequest>();
        private int _people;
        private DateTime _startDate;
        private TimeSpan _startTime, _endTime;
        private string _presentStartDate, _presentStartTime, _presentEndTime;

        public void SetDisplayDateTime()
        {
            _presentStartDate = _startDate.ToString("dd MMM");

            var convertTimes = Convert.ToDateTime(_startTime.ToString());

            _presentStartTime = convertTimes.ToString("h:mm tt");

            convertTimes = Convert.ToDateTime(_endTime.ToString());

            _presentEndTime = convertTimes.ToString("h:mm tt");
        }

        public int RequestID { get; set; }

        public string RequestedBy { get; set; }

        public string Purpose { get; set; }

        public int People
        {
            get => _people;

            set
            {
                if (value < 0)

                    throw new Exception("Number of people must be greater than 0");

                if (value > _seatingCapacity)

                    throw new Exception("Number of people cannot exceed seatingNumber capacity");

                _people = value;
            }
        }

        public BookingRequest(string roomNumber, int seatingNumber, RoomLayoutType.RoomLayoutTypeEnum room, string requestedBy, string description,
            int people, DateTime startDate, TimeSpan startTime, TimeSpan endTime) : base(roomNumber, seatingNumber, room)
        {

            RequestID = _requestList.Count + 1;

            RequestedBy = requestedBy;

            Purpose = description;

            People = people;

            _startDate = startDate;

            _startTime = startTime;

            _endTime = endTime;

            SetDisplayDateTime();

            if (_startTime > _endTime)
                throw new Exception("Starting time cannot be set after ending time");

        }

        public string RequestedRoomOutput => $"Request by {RequestedBy} for the purpose of {Purpose} on {_presentStartDate} from {_presentStartTime} to {_presentEndTime}";

    }
}
