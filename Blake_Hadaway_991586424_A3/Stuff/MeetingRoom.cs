﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blake_Hadaway_991586424_A3.Stuff
{
    public class MeetingRoom
    {

        public string RoomDescription => $"Room {_roomNumber} with {_roomLayout} layout that can hold {_seatingCapacity} guests";

        public string ImagePathway => $"assets/Pictures/{_roomLayout}.png";

        public string _roomNumber;
        public int _seatingCapacity;
        public RoomLayoutType.RoomLayoutTypeEnum _roomLayout;


        public MeetingRoom(string roomNum, int seating, RoomLayoutType.RoomLayoutTypeEnum room)
        {
            _roomNumber = roomNum;
            _seatingCapacity = seating;
            _roomLayout = room;
        }

        
    }
}

